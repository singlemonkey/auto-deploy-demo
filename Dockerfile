FROM openjdk:17
LABEL authors="zhangqi"

WORKDIR /app

COPY target/auto-deploy-demo.jar /app/auto-deploy-demo.jar

EXPOSE 9032

CMD ["java","-jar","auto-deploy-demo.jar","--spring.profiles.active=${PROFILE}"]