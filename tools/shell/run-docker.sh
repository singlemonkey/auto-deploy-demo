#!/bin/bash

shopt -s nocasematch

echo "检测docker compose版本"
docker_compose_cmd=""
docker_compose_output=$(docker-compose --version 2>&1)
if [[ "$docker_compose_output" == *"docker-compose version"* ]]; then
  echo "安装的是 Docker Compose:"
  docker-compose --version
  docker_compose_cmd="docker-compose"
else
  # 检查docker compose版本
  docker_compose_cli_output=$(docker compose version 2>&1)
  if [[ "$docker_compose_cli_output" == *"docker compose version"* ]]; then
    echo "安装的是 Docker Compose CLI:"
    docker compose version
    docker_compose_cmd="docker compose"
  else
    echo "未安装 Docker Compose 或 Docker Compose CLI."
  fi
fi

echo "---请确认已正确配置env文件环境变量内容---"

if [ -n "$docker_compose_cmd" ]; then
  echo "停止现有应用服务"
  $docker_compose_cmd down
  echo "清理同版本镜像"
  docker rmi $(docker images | grep project-name | awk '{print $3}')
  echo "检测服务器架构"
  arch=$(uname -m)
  # 判断是否为ARM架构
  if [ "$arch" == "aarch64" ]; then
    echo "ARM架构服务器，加载ARM镜像：project-name-arm64.tar"
    docker load <project-name-arm64.tar
  else
    echo "非ARM架构服务器,加载AMD镜像：project-name-amd64.tar"
    docker load <project-name-amd64.tar
  fi
  echo "启动应用"
  $docker_compose_cmd up -d
  echo "应用启动完成，查看日志：docker logs project-name --tail 100 -f"
else
  echo "未知的docker compose 版本"
fi
