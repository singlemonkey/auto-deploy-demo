package com.autodeploy.demo.controller;

import com.autodeploy.demo.config.SystemSetting;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
@Slf4j
public class ApiController {

    private final SystemSetting systemSetting;

    @GetMapping("/getSystemSetting")
    public SystemSetting getAppName() {

        log.debug("系统版本号：" + systemSetting.getAppVersion());
        log.debug("数据库地址：" + systemSetting.getDbDemoHost());
        log.debug("数据库端口号：" + systemSetting.getDbDemoPort());
        log.debug("短信服务商：" + systemSetting.getSmsServerType());
        log.debug("短信服务地址：" + systemSetting.getSmsServerHost());

        return systemSetting;
    }
}
