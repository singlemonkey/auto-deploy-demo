package com.autodeploy.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "system-setting")
public class SystemSetting {
    private String dbDemoHost;
    private String dbDemoPort;
    private String appVersion;
    private String smsServerType;
    private String smsServerHost;
}
